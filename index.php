<!-- https://gitlab.com/fr0stb1rd/fbbw-0002 -->

<?php

session_start();
if(!isset($_SESSION['username'])){
    header("location:account/login.php");
}

?>



<!doctype html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Welcome Home</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.3/dist/css/bootstrap.min.css" rel="stylesheet" 
    integrity="sha384-QWTKZyjpPEjISv5WaRU9OFeRpok6YctnYmDr5pNlyT2bRjXh0JMhjY6hW+ALEwIH" crossorigin="anonymous">
    <style>
      .fixed-size {
        width: 100%;
        height: 200px;
        object-fit: cover;
      }
    </style>
  </head>
  <body>
    <div class="container mt-5">
        <h1 class="text-center">Welcome 
            <?php 
            echo $_SESSION['username'];
            ?>
        </h1>
        
        <div class="text-center mt-5">
            <h2>Gizli Bilgiler</h2>
            <p><strong>Görev:</strong> Bu bilgiler yüksek derecede gizlidir ve sadece yetkili kişiler tarafından görüntülenebilir.</p>
            <p><strong>Proje Omega:</strong> Yeni nesil teknoloji geliştirme projesi.</p>
            <p><strong>Operasyon Kodu:</strong> FR0STB1RD. Lütfen bu kodu kimseyle paylaşmayın.</p>
            <p><strong>Şifreli Mesaj:</strong> "Mavi kuş sabah uçar, kırmızı tilki geceyi bekler."</p>
            <p><strong>Gizli Toplantı:</strong> Çarşamba günü saat 13:13'te, Büyük Kütüphane'nin altındaki gizli odada buluşalım.</p>
        </div>

        <div class="row mt-5">
            <div class="col-md-4 mb-4">
                <img src="img/gizemli0.jpg" class="img-fluid rounded fixed-size" alt="Secret Document">
                <p class="mt-2 text-center">Gizli Belge 42: Asla pizza yemeden önce okumayın.</p>
                <ul>
                    <li>Kedilerin gizli tapınağı</li>
                    <li>Pandaların dans yarışması</li>
                    <li>Gizli ajan tavşanlar</li>
                    <li>Uzaylıların çay partisi</li>
                    <li>Görünmez köpekler</li>
                </ul>
            </div>
            <div class="col-md-4 mb-4">
                <img src="img/gizemli1.jpg" class="img-fluid rounded fixed-size" alt="Mystery Location">
                <p class="mt-2 text-center">Gizemli Mekan: Burada daha önce hiç kimse kahve içmedi.</p>
                <ul>
                    <li>Zamanda yolculuk yapan fareler</li>
                    <li>Görünmezlik pelerini</li>
                    <li>Konuşan dinozorlar</li>
                    <li>Yeraltı karınca şehirleri</li>
                    <li>Uçan penguenler</li>
                </ul>
            </div>
            <div class="col-md-4 mb-4">
                <img src="img/gizemli2.jpg" class="img-fluid rounded fixed-size" alt="Top Secret Meeting">
                <p class="mt-2 text-center">Gizli Toplantı: Kedilerin dünyayı ele geçirme planı burada tartışıldı.</p>
                <ul>
                    <li>Yıldızlara gitmiş inekler</li>
                    <li>Telepatik kaplumbağalar</li>
                    <li>Havlayan ördekler</li>
                    <li>Fısıldayan filler</li>
                    <li>Zıplayan balinalar</li>
                </ul>
            </div>
            <div class="col-md-4 mb-4">
                <img src="img/gizemli3.jpg" class="img-fluid rounded fixed-size" alt="Encrypted Message">
                <p class="mt-2 text-center">Şifreli Mesaj: Eğer bunu okuyorsan, çok yaklaştın demektir.</p>
                <ul>
                    <li>Sihirli tavuklar</li>
                    <li>Robot köpekler</li>
                    <li>Dans eden filler</li>
                    <li>Şarkı söyleyen balıklar</li>
                    <li>Zihin okuyan kargalar</li>
                </ul>
            </div>
            <div class="col-md-4 mb-4">
                <img src="img/gizemli4.jpg" class="img-fluid rounded fixed-size" alt="Classified Project">
                <p class="mt-2 text-center">Gizli Proje: Bu sadece bir boş sayfa, ama kimse bilmiyor.</p>
                <ul>
                    <li>Parlayan baykuşlar</li>
                    <li>Gizli konuşan ağaçlar</li>
                    <li>Şifreli kedi mırıldanmaları</li>
                    <li>Zamanı durduran köpekler</li>
                    <li>Görünmez tavşanlar</li>
                </ul>
            </div>
            <div class="col-md-4 mb-4">
                <img src="img/gizemli5.jpg" class="img-fluid rounded fixed-size" alt="Classified Project">
                <p class="mt-2 text-center">Gizli Proje: Bu sadece bir boş sayfa, ama kimse bilmiyor.</p>
                <ul>
                    <li>Parlayan baykuşlar</li>
                    <li>Gizli konuşan ağaçlar</li>
                    <li>Şifreli kedi mırıldanmaları</li>
                    <li>Zamanı durduran köpekler</li>
                    <li>Görünmez tavşanlar</li>
                </ul>
            </div>
        </div>

        <div class="d-flex justify-content-center mt-5">
            <a href="account/logout.php" class="btn btn-primary mx-2">Logout</a>
            <a href="account/delete.php" class="btn btn-secondary mx-2">Delete Account</a>
        </div>
    </div>

    <!-- https://gitlab.com/fr0stb1rd/fbbw-0002 -->
    <?php include "inc/footer.php" ?>
  </body>
</html>
