<!-- https://gitlab.com/fr0stb1rd/fbbw-0002 -->

<?php

$success = 0;
$invalid = 0;

if($_SERVER['REQUEST_METHOD'] == 'POST'){
    include "../db/connect.php";

    $username = $_POST['username'];
    $password = $_POST['password'];

    $sql="select * from `registration` where username='$username' and password='$password'";
    $result=mysqli_query($con, $sql);
    if($result){
        if(mysqli_num_rows($result)>0){
            $success=1;
        }
        else{
            $invalid=1;
        }
    }
    if($success){
        session_start();
        $_SESSION['username']=$username;
        header('location:../index.php');
    }
}
?>


<!doctype html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Login</title>
    <h1 class="text-center">Login Page</h1>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.3/dist/css/bootstrap.min.css" rel="stylesheet" 
    integrity="sha384-QWTKZyjpPEjISv5WaRU9OFeRpok6YctnYmDr5pNlyT2bRjXh0JMhjY6hW+ALEwIH" crossorigin="anonymous">
  </head>
  <body>

    <?php 
    
    if($success){
        echo '
            <div class="alert alert-info mt-5 text-center" role="alert">
            You logged in now.
            </div>
            ';
    }
    
    else if($invalid){
        echo '
            <div class="alert alert-danger mt-5 text-center" role="alert">
            Invalid login.
            </div>
            ';
    }
    
    ?>

    <div class="container mt-5">

        <form action="login.php" method="post">
        <div class="row mb-3">
            <label for="inputEmail3" class="col-sm-2 col-form-label">Email</label>
            <div class="col-sm-10">
            <input type="text" class="form-control" name="username">
            </div>
        </div>
        <div class="row mb-3">
            <label for="inputPassword3" class="col-sm-2 col-form-label">Password</label>
            <div class="col-sm-10">
            <input type="password" class="form-control" name="password">
            </div>
        </div>
        
        <button type="submit" class="btn btn-primary w-100">Login</button>
        </form>
        <div class="alert alert-info mt-5 text-center" role="alert">
            Do not have account? <a href="signup.php">Make new here</a>
        </div>

    </div>
  </body>
  <?php include "../inc/footer.php" ?>
</html>

