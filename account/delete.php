<!-- https://gitlab.com/fr0stb1rd/fbbw-0002 -->

<?php
session_start();
# eğer session yoksa logine at
if(!isset($_SESSION['username'])){
    // echo "sessionda username yok";
    header("location:login.php");
}

$success = 0;
$invalid = 0;

if($_SERVER['REQUEST_METHOD'] == 'POST'){
    
    include "../db/connect.php";

    $username = $_SESSION['username'];
    $password = $_POST['password'];

    $sql="delete from `registration` where username='$username' and password='$password'";
    $result=mysqli_query($con, $sql);
    if($result){
        $affectedRows = mysqli_affected_rows($con);
        if ($affectedRows > 0) {
            $success=1;
        } else {
            $invalid=1;
        }
    }
}
?>


<!doctype html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Account Deletion</title>
    <h1 class="text-center">Account Deletion Page</h1>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.3/dist/css/bootstrap.min.css" rel="stylesheet" 
    integrity="sha384-QWTKZyjpPEjISv5WaRU9OFeRpok6YctnYmDr5pNlyT2bRjXh0JMhjY6hW+ALEwIH" crossorigin="anonymous">
  </head>
  <body>
    <div class="alert alert-danger mt-5 text-center" role="alert">
        Your all data will be erased from our servers. If you want to continue please enter your password again.
    </div>

    <?php 
    
    if($success){
        echo '
            <div class="alert alert-info mt-5 text-center" role="alert">
            Your account deleted succesfully.
            </div>
            ';
        session_destroy();
    }
    
    else if($invalid){
        echo '
            <div class="alert alert-danger mt-5 text-center" role="alert">
            Wrong data. Cannot delete account.
            </div>
            ';
    }
    
    ?>

    <div class="container mt-5">

        <form action="delete.php" method="post">
        
        <div class="row mb-3">
            <label for="inputPassword3" class="col-sm-2 col-form-label">Password</label>
            <div class="col-sm-10">
            <input type="password" class="form-control" name="password">
            </div>
        </div>
        
        <button type="submit" class="btn btn-danger w-100">Delete My Account</button>
        <a class="btn btn-success w-100 mt-5" href="../index.php" role="button">Back to Home</a>

        </form>


    </div>
  </body>
  <?php include "../inc/footer.php" ?>
</html>


