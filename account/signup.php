<!-- https://gitlab.com/fr0stb1rd/fbbw-0002 -->

<?php

$success=0;
$user_already_exist=0;
$user_blank=0;
$password_blank=0;

if($_SERVER['REQUEST_METHOD'] == 'POST'){
    

    $username = $_POST['username'];
    $password = $_POST['password'];

    if(strlen($username)==0){
        $user_blank=1;

    }
    if(strlen($password)==0){
        $password_blank=1;

    }
    if((!$user_blank) && (!$password_blank)){
        include "../db/connect.php";
        $sql="select * from `registration` where username='$username'";
        $result=mysqli_query($con, $sql);
        if($result){
            $num=mysqli_num_rows($result);
            if($num>0){
                // echo "$username already exist";
                $user_already_exist=1;
            }
            else{
                $sql="insert into `registration`(username,password) values('$username','$password')";
                $result=mysqli_query($con, $sql);
                if($result){
                    // echo "sugnup successful.";
                    $success=1;
                }
                else{
                    die(mysqli_error($con));
                }
            }
        }
    }
    
    
}
?>


<!doctype html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Signup</title>
    <h1 class="text-center">Signup Page</h1>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.3/dist/css/bootstrap.min.css" rel="stylesheet" 
    integrity="sha384-QWTKZyjpPEjISv5WaRU9OFeRpok6YctnYmDr5pNlyT2bRjXh0JMhjY6hW+ALEwIH" crossorigin="anonymous">
  </head>
  <body>

    <?php 
    
    if($user_blank){
        echo '
            <div class="alert alert-danger mt-5 text-center" role="alert">
            username cannot be empty
            </div>
            ';
    }

    else if($password_blank){
        echo '
            <div class="alert alert-danger mt-5 text-center" role="alert">
            password cannot be empty
            </div>
            ';
    }

    else if($user_already_exist){
        echo '
            <div class="alert alert-danger mt-5 text-center" role="alert">
            User already exist. <a href="login.php">Login?</a>
            </div>
            ';
    }
    
    else if($success){
        echo '
            <div class="alert alert-info mt-5 text-center" role="alert">
            You are successfully signed up. <a href="login.php">Login?</a>
            </div>
            ';
    }
    
    ?>

    <div class="container mt-5">
        

        <form action="signup.php" method="post">
        <div class="row mb-3">
            <label for="inputEmail3" class="col-sm-2 col-form-label">Email</label>
            <div class="col-sm-10">
            <input type="text" class="form-control" name="username">
            </div>
        </div>
        <div class="row mb-3">
            <label for="inputPassword3" class="col-sm-2 col-form-label">Password</label>
            <div class="col-sm-10">
            <input type="password" class="form-control" name="password">
            </div>
        </div>
        
        <button type="submit" class="btn btn-primary w-100">Signup</button>
        </form>

        <div class="alert alert-info mt-5 text-center" role="alert">
            Do you have account? <a href="login.php">Login here</a>
        </div>

    </div>
  </body>
  <!-- <?php include "../inc/footer.php" ?> -->
</html>

