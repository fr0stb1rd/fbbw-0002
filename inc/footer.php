<!-- https://gitlab.com/fr0stb1rd/fbbw-0002 -->

<footer class="bg-dark text-white mt-5">
    <div class="container py-4">
        <div class="text-center mt-3">
            <p>&copy; 2024 fr0stb1rd <a style="text-decoration: none;" 
            href="https://gitlab.com/fr0stb1rd/fbbw-0002">fbbw-0002</a>. Tüm hakları saklıdır.</p>
        </div>
    </div>
</footer>